package eu.eisti.maths.polynomial;

/**
 * The type Monomial.
 */
public class Monomial {
    private final int degree;
    private final double coeff;

    /**
     * Gets coeff.
     *
     * @return the coeff
     */
    public double getCoeff() {
        return coeff;
    }

    /**
     * Gets degree.
     *
     * @return the degree
     */
    public int getDegree() {
        return degree;
    }

    /**
     * Instantiates a new Monome.
     *
     * @param degree the degree
     * @param coeff  the coeff
     */
    public Monomial(int degree, double coeff) {
        this.degree = degree;
        this.coeff = coeff;
    }

    /**
     * Evalue double.
     *
     * @param m the m
     * @param x the x
     * @return the double
     */
    public static double evalue(Monomial m, double x) {
        return m.getCoeff() * Math.pow(x, m.getDegree()); //f(x) = a * x^n
    }

    /**
     * Derive monome.
     *
     * @param m the m
     * @return the monome
     */
    public static Monomial derive(Monomial m) {
        return new Monomial(m.getDegree() - 1, m.getDegree()*m.getCoeff()); //(ax^n)' = a*n*x^(n-1)
    }

    @Override
    public String toString() {
        var sign = (getCoeff() < 0 )?"- ":"+ ";
        var degree = (getDegree() == 0) ? "" : ((getDegree() == 1) ? "x" : ("x^" + getDegree()));
        return sign + getCoeff() + degree;
    }
}
