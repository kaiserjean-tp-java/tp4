package eu.eisti.maths.polynomial;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * The type Polynomial.
 */
public class Polynomial {
    private final Monomial[] polynomial;

    /**
     * Instantiates a new Polynomial.
     *
     * @param polynomial the polynomial
     */
    public Polynomial(Monomial[] polynomial) {
        this.polynomial = Arrays.stream(polynomial)
                .parallel()
                .filter(monomial -> monomial.getDegree() >= 0) //we want to avoid below 0 degree for monomials
                .sorted(
                        Comparator
                                .comparingInt(Monomial::getDegree)
                                .reversed() //we want polynomial to have his greater monomial (in term of power) at index 0
                )
                .toArray(Monomial[]::new);
    }

    /**
     * Gets degree.
     *
     * @param p the p
     * @return the degree
     */
    public static int getDegree(Polynomial p) {
        return p.polynomial[0].getDegree(); //with our constructor we are assured to have our greatest degree monomial in index 0
    }

    /**
     * Get double.
     *
     * @param p      the p
     * @param degree the degree
     * @return the double
     */
    public static double get(Polynomial p, int degree) {
        return p.polynomial[degree].getCoeff(); //with our constructor we can have that pretty efficient getter
    }

    /**
     * Get polynomial monome [ ].
     *
     * @return the monome [ ]
     */
    public Monomial[] getPolynomial() {
        return polynomial;
    }

    /**
     * Evaluate p(x).
     *
     * @param p a polynomial
     * @param x a value to evaluate p with
     * @return the result of this evaluation
     */
    public static double evalue(Polynomial p, double x) {
        /*
        * evaluation of a polynomial is :
        * P(x) = Σ(i = 0 to n) M_i(x) with M_i being monomial of degree i
        * */
        Function<Monomial, Double> evaluator = monomial -> Monomial.evalue(monomial, x);
        return Arrays.stream(p.polynomial)
                .parallel()
                .map(evaluator) // M_i(x)
                .reduce(Double::sum) //Σ(i = 0 to n)
                .orElseThrow();
    }

    /**
     * Derive polynomial.
     *
     * @param p the p
     * @return the polynomial
     */
    public static Polynomial derive(Polynomial p) {
        /*
        * one property of derivation is : (x+y)' = x' + y'
        * so : P'(X) = (Σ(i = 0 to n) M_i(X))' = Σ(i = 0 to n) M_i'(X) with M_i being monomial of degree i
        * */
        return new Polynomial(
                Arrays.stream(p.polynomial)
                        .parallel()
                        .map(Monomial::derive) // M_i'(X)
                        .toArray(Monomial[]::new)
        ); //we use our constructor to keep consistency after derivation (because derivation could imply negative degrees) and avoid unused array slot
    }

    @Override
    public String toString() {
        return Arrays.stream(polynomial)
                .map(Monomial::toString)
                .collect(Collectors.joining(" "));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Polynomial polynomial1 = (Polynomial) o;
        return Arrays.equals(polynomial, polynomial1.polynomial);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(polynomial);
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        var m1 = new Monomial(2, 4);
        var m2 = new Monomial(1, 2);
        var m3 = new Monomial(0, 15);
        var p = new Polynomial(new Monomial[]{m1, m2, m3});
        System.out.println("p' = " + Polynomial.derive(p));
    }
}
