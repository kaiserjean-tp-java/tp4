package eu.eisti.maths;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * The type Matrix.
 */
public class Matrix {

    private final double[][] coeffs;

    /**
     * Instantiates a new Matrix.
     *
     * @param nbRows    the nb rows
     * @param nbColumns the nb columns
     */
    public Matrix(int nbRows, int nbColumns) {
        if (nbRows > 0 && nbColumns > 0) {
            this.coeffs = new double[nbRows][nbColumns];
            Arrays.stream(this.coeffs).forEach(row -> Arrays.fill(row, 0.0));
        } else {
            this.coeffs = new double[0][0];
        }
    }

    /**
     * Instantiates a new Matrix.
     *
     * @param m the m
     */
    public Matrix(double[][] m) {
        this.coeffs = new double[m.length][m[0].length];
        IntStream.range(0, m.length).forEach(i -> System.arraycopy(m[i], 0, this.coeffs[i], 0, m[i].length));
    }

    /**
     * Instantiates a new Matrix.
     *
     * @param m the m
     */
    public Matrix(Matrix m) {
        this(m.coeffs);
    }

    /**
     * Gets nb rows.
     *
     * @return the nb rows
     */
    public int getNbRows() {
        return this.coeffs.length;
    }

    /**
     * Gets nb columns.
     *
     * @return the nb columns
     */
    public int getNbColumns() {
        if (this.getNbRows() > 0) {
            return this.coeffs[0].length;
        } else {
            return 0;
        }
    }

    /**
     * Get double.
     *
     * @param row    the row
     * @param column the column
     * @return the double
     */
    public double get(int row, int column) {
        if (row < 0 || row >= this.getNbRows() || column < 0
                || column >= this.getNbColumns()) {
            return Double.POSITIVE_INFINITY;
        } else {
            return this.coeffs[row][column];
        }
    }

    /**
     * Set.
     *
     * @param row    the row
     * @param column the column
     * @param value  the value
     */
    public void set(int row, int column, double value) {
        if (row >= 0 && row < this.getNbRows() && column >= 0
                && column < this.getNbColumns()) {
            this.coeffs[row][column] = value;
        }
    }

    /**
     * Sum double.
     *
     * @param m the m
     * @return the double
     */
    public static double sum(Matrix m) {
        return Stream.of(m.coeffs)
                .parallel()
                .flatMapToDouble(DoubleStream::of) //question : is flatMap keeping the "parallelized behaviour" of the pipeline ?
                .sum();
    }

    /**
     * Is producible boolean.
     *
     * @param m1 the m 1
     * @param m2 the m 2
     * @return the boolean
     */
    public static boolean isProducible(Matrix m1, Matrix m2) {
        return m1.getNbColumns() == m2.getNbRows();
    }

    /**
     * Perform product matrix.
     *
     * @param m1 the m 1
     * @param m2 the m 2
     * @return the matrix
     */
    public static Matrix performProduct(Matrix m1, Matrix m2) {
        //it's a classic algorithm, no special use of Java features here
        var product = new double[m1.getNbRows()][m2.getNbColumns()];
        for (int i = 0; i < product.length; i++) {
            for (int j = 0; j < product[0].length; j++) {
                for (int k = 0; k < m1.getNbColumns(); k++) {
                    product[i][j] = product[i][j] + (m1.get(i, k) * m2.get(k, j));
                }
            }
        }
        return new Matrix(product);
    }

    /**
     * Product matrix.
     *
     * @param m1 the m 1
     * @param m2 the m 2
     * @return the matrix
     */
    public static Matrix product(Matrix m1, Matrix m2) {
        Matrix product;
        if (!isProducible(m1, m2) && !isProducible(m2, m1)) { //first check if product is doable
            return null; //returning an Optional is better than a null but consigns are consigns
        }
        if (isProducible(m1,m2)) { //this if-else statement helps factorising code by calling the same method in both cases
            product = performProduct(m1, m2);
        } else {
            product = performProduct(m2, m1);
        }
        return product;
    }

    /**
     * All positive boolean.
     *
     * @param m a matrix
     * @return true if all values are positives, false otherwise
     */
    public static boolean allPositive(Matrix m) {
        return Arrays.stream(m.coeffs)
                .parallel()
                .flatMapToDouble(DoubleStream::of) //transform 2D array to 1D array, we don't care about position, only values
                .allMatch(coeff -> coeff > 0); //we check if all elements match this property
    }

    @Override
    public String toString() {
        return Arrays.stream(this.coeffs) //these nested pipelines are quite ugly, could be improved with better knowledge of the Stream API
                .map(
                        row -> Arrays.stream(row)
                                .mapToObj(Double::toString)
                                .collect(Collectors.joining(" | "))
                )
                .collect(Collectors.joining("\n"));
    }

}